1. ## Goal:

   - To build a web service in Rust and use docker file to containerize the file and incorporate CI/CD configurations.

   ## Demo Video:

   - [Demo Video](https://gitlab.com/ethan-huang/fh89_indiproject_2/-/blob/master/demoid2.mp4?ref_type=heads)

   # Steps:

   - `cargo new <project>` to start a new project

   - Go to `src/main.rs` to implement a web service in Rust to show the users whether a number is prime or not

   - `cargo run` and go to localhost:8080 to test the functionality of the web service

   - Make a newfile called Dockerfile and build the steps to build an image

   - Run `docker build -t <image_name>` and `docker run -p 8080:8080 <YOUR IMAGE NAME>`

   - Add .gitlab-ci.yml file for pipeline for the CI/CD configuration

   # Result

   - docker ![dockerfile](/images/docker.png)

   - the input value ![input](/images/res1.png)

   - result of the input value![result](/images/res2.png)
