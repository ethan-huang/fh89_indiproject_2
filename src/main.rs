use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use std::str::FromStr;

fn is_prime(n: u32) -> bool {
    if n <= 1 {
        return false;
    }
    for i in 2..=(n as f64).sqrt() as u32 {
        if n % i == 0 {
            return false;
        }
    }
    true
}

async fn check_prime(number: web::Form<FormData>) -> impl Responder {
    let num = number.number;
    let is_prime = is_prime(num);
    let result = if is_prime { "Prime" } else { "Not Prime" };
    HttpResponse::Ok().body(result)
}

async fn index() -> impl Responder {
    HttpResponse::Ok().body(
        r#"
        <html>
            <head><title>Prime Checker</title></head>
            <body>
                <h1>Prime Checker</h1>
                <form action="/check_prime" method="post">
                    <label for="number">Enter a number:</label>
                    <input type="text" id="number" name="number">
                    <button type="submit">Check</button>
                </form>
            </body>
        </html>
        "#,
    )
}

#[derive(serde::Deserialize)]
struct FormData {
    number: u32,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(web::resource("/").route(web::get().to(index)))
            .service(web::resource("/check_prime").route(web::post().to(check_prime)))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
